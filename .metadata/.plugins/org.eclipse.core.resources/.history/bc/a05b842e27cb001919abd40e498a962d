package com.ztech.db;

package com.ztech.kra.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import com.ztech.demo.constants.*;

public class DBConnection {
	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;

	private static final Logger LOGGER = Logger.getLogger(DBConnection.class.getName());

	public DBConnection() {

	}

	public Connection openConnection() throws ApplicationException {
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(DBConfig.URL, DBConfig.USER_NAME,
					DBConfig.PASSWORD);

		} catch (SQLException e) {
			LOGGER.info(e.getMessage());
			throw new ApplicationException(ErrorConstants.ERROR);

		} catch (ClassNotFoundException e) {
			LOGGER.info(e.getMessage());
			throw new ApplicationException(ErrorConstants.ERROR);

		}
		return connection;
	}

	public void close(Statement statement) throws ApplicationException {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOGGER.info(e.getMessage());
				throw new ApplicationException(ErrorConstants.TECHNICAL_ERROR);

			}
		}

	}

	public void close(ResultSet resultSet) throws ApplicationException {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				LOGGER.info(e.getMessage());
				throw new ApplicationException(ErrorConstants.TECHNICAL_ERROR);

			}
		}

	}

	public void close(Connection connection) throws ApplicationException {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				LOGGER.info(e.getMessage());
				throw new ApplicationException(ErrorConstants.TECHNICAL_ERROR);
			}
		}
	}

	public void close(PreparedStatement preparedStatement) throws ApplicationException {
		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				LOGGER.info(e.getMessage());
				throw new ApplicationException(ErrorConstants.ERROR);

			}
		}

	}

	public void close(DBConnection connectionClassObj) throws ApplicationException {
		if (connectionClassObj != null) {
			try {
				connectionClassObj.close(connection);
			} catch (Exception e) {
				LOGGER.info(e.getMessage());
				throw new ApplicationException(ErrorConstants.TECHNICAL_ERROR);
			}
		}
		
	}

}
