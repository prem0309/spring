package com.ztech.constants;

public class ErrorConstants {

	public static final String LOGIN_ERROR = "An error has occured during login please try again after some time.";

	public static final String POPULATE_ERROR = "An error has occured, please try again later.";

	public static final String SET_PASSWORD_ERROR = "An error has occured, please reset the password after some time.";

	public static final String TECHNICAL_ERROR = "A technical error has occured please try after some time.";

	public static final String ERROR_SQLEXCEPTION = "THERE IS A PROBLEM WITH DATABASE ACCESS, PLEASE TRY AGAIN LATER";
	
	public static final String ERROR_CLASSNOTFOUNDEXCEPTION = "THERE IS A PROBLEM WITH DATABASE CONNECTIVITY, PLEASE TRY AGAIN LATER";
	
	public static final String ERROR_EXCEPTION = "MISMATCHED INPUT ENCOUNTERED, ENTER VALID INPUT";
	
	public static final String ERROR_AES_EXCEPTION = "PROBLEM OCCURRED IN RETREIVING USER DETAILS, PLEASE TRY AGAIN LATER";
	
	public static final String ERROR_NOSUCHALGORITHMEXCEPTION = "PROBLEM OCCURRED AT BACKEND COMPUTATION,PLEASE TRY AGAIN";
	
	public static final String ERROR_MESSAGINGEXCEPTION = "MAIL HAD NOT SEND DUE TO UNFORTUNATE SITUATIONS,PLEASE TRY AGAIN";

	public static final String ERROR = "SOMETHING PROBLEM HAS OCCURRED, PLEASE TRY AGAIN LATER";
	
}
