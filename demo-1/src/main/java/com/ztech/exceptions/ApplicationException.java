package com.ztech.exceptions;

public class ApplicationException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	private String errorMessage;
	
	public ApplicationException(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
		
}
