package com.ztech.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import com.ztech.constants.ErrorConstants;
import com.ztech.db.DBConnection;
import com.ztech.exceptions.ApplicationException;
import com.ztech.model.Demo;


public class DemoDAO {

	DBConnection connect;
	Connection connection = null;
	PreparedStatement prepareStmt = null;
	ResultSet set = null;
	ArrayList<Demo> set_items;
	ArrayList<Demo> set_itemsById;
	
	public DemoDAO(){
		connect = new DBConnection();		
	}
	
	


	public String add(Demo demoResponse) throws ApplicationException {
		try {
			 connection = (Connection) connect.openConnection();
			if (connection != null) {

				prepareStmt  = connection.prepareStatement("Insert into Demo(eid,fname,lname,email,phone,salary) values(?,?,?,?,?,?)");
				prepareStmt.setInt(1,demoResponse.getEid());
				prepareStmt.setString(2,demoResponse.getFname());
				prepareStmt.setString(3,demoResponse.getLname());
				prepareStmt.setString(4,demoResponse.getEmail());
				prepareStmt.setString(5,demoResponse.getPhone());
				prepareStmt.setLong(6,demoResponse.getSalary());
				
				if(prepareStmt.executeUpdate()!= 0) {
					return "ADDED SUCCESSFULLY";
				}
				else {
					return "ERROR";
				}
				

				
			}
		} catch (SQLException e) {


			throw new ApplicationException(ErrorConstants.ERROR_SQLEXCEPTION);

		} finally {

			connect.close(prepareStmt);
			connect.close(connection);

		}

		return null;
	
		
		
		
	}

	public ArrayList<Demo> display() throws ApplicationException {
		
		set_items = new ArrayList<>();
		try {
			 connection = (Connection) connect.openConnection();
			if (connection != null) {

				prepareStmt  = connection.prepareStatement("select eid,fname,lname,email,phone,salary from Demo");
				set  = prepareStmt.executeQuery();

				while (set.next()) {
					Demo setModel = new Demo();
					setModel.setEid(set.getInt(1));
					setModel.setFname(set.getString(2));
					setModel.setLname(set.getString(3));
					setModel.setEmail(set.getString(4));
					setModel.setPhone(set.getString(5));
					setModel.setSalary(set.getInt(6));

					set_items.add(setModel);
				}

				if (set_items.size() > 0) {

					return set_items;
				}
			}
		} catch (SQLException e) {


			throw new ApplicationException(e+"  "+ErrorConstants.ERROR_SQLEXCEPTION);

		} finally {

			connect.close(set);
			connect.close(prepareStmt);
			connect.close(connection);

		}

		return null;
	}


	public ArrayList<Demo> displayById(int eid) throws ApplicationException {
		
		set_itemsById = new ArrayList<>();
		try {
			 connection = (Connection) connect.openConnection();
			if (connection != null) {

				prepareStmt  = connection.prepareStatement("select eid,fname,lname,email,phone,salary from Demo where eid=?");
				prepareStmt.setInt(1, eid); 
				set  = prepareStmt.executeQuery();

				while (set.next()) {
					Demo setModel = new Demo();
					setModel.setEid(set.getInt(1));
					setModel.setFname(set.getString(2));
					setModel.setLname(set.getString(3));
					setModel.setEmail(set.getString(4));
					setModel.setPhone(set.getString(5));
					setModel.setSalary(set.getInt(6));

					set_itemsById.add(setModel);
				}

				if (set_itemsById.size() > 0) {

					return set_itemsById;
				}
			}
		} catch (SQLException e) {


			throw new ApplicationException(e+"  "+ErrorConstants.ERROR_SQLEXCEPTION);

		} finally {

			connect.close(set);
			connect.close(prepareStmt);
			connect.close(connection);

		}

		return null;
	}

	public String update(int eid, Demo demoResponse) throws ApplicationException {
		try {
			 connection = (Connection) connect.openConnection();
			if (connection != null) {

				prepareStmt  = connection.prepareStatement("Update Demo set eid=?,fname=?,lname=?,email=?,phone=?,salary=? where eid=?");
				prepareStmt.setInt(1,demoResponse.getEid());
				prepareStmt.setString(2,demoResponse.getFname());
				prepareStmt.setString(3,demoResponse.getLname());
				prepareStmt.setString(4,demoResponse.getEmail());
				prepareStmt.setString(5,demoResponse.getPhone());
				prepareStmt.setLong(6,demoResponse.getSalary());
				prepareStmt.setInt(7,eid);
				
				if(prepareStmt.executeUpdate()!= 0) {
					return "UPDATED SUCCESSFULLY";
				}
				else {
					return "ERROR";
				}
				

				
			}
		} catch (SQLException e) {


			throw new ApplicationException(e+" "+ErrorConstants.ERROR_SQLEXCEPTION);

		} finally {

			connect.close(prepareStmt);
			connect.close(connection);

		}

		return null;

	}

	public String delete(int eid) throws ApplicationException {
		try {
			 connection = (Connection) connect.openConnection();
			if (connection != null) {

				prepareStmt  = connection.prepareStatement("Delete from Demo where eid=?");
				prepareStmt.setInt(1,eid);
				
				
				if(prepareStmt.executeUpdate()!= 0) {
					return "DELETED SUCCESSFULLY";
				}
				else {
					return "ERROR";
				}
				

				
			}
		} catch (SQLException e) {


			throw new ApplicationException(ErrorConstants.ERROR_SQLEXCEPTION);

		} finally {

			connect.close(prepareStmt);
			connect.close(connection);

		}

		return null;
	
	}
	
	
	

}
