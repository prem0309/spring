package com.ztech.delegate;

import java.util.ArrayList;

import com.ztech.dao.DemoDAO;
import com.ztech.exceptions.ApplicationException;
import com.ztech.model.Demo;


public class DemoDelegate {

	DemoDAO daoObject;
	
	public DemoDelegate(){
		 daoObject = new DemoDAO();
	}
	
	ArrayList<Demo> demo = new ArrayList<>();


	public String addEmployee(Demo demoResponse) {

		try {
			return daoObject.add(demoResponse);
		} catch (ApplicationException e) {
			System.out.println("unfortunate "+e.getErrorMessage());
		}
		return null;
	}

	public ArrayList<Demo> displayEmployee() {

		try {
			demo = daoObject.display();

		} catch (ApplicationException e) {

			System.out.println("Display -unfortunate"+e.getErrorMessage());
		}
		return demo;

	}

	public ArrayList<Demo> displayEmployeeById(int eid) {
		try {
			demo = daoObject.displayById(eid);
			if(demo == null) {
			}

		} catch (ApplicationException e) {

			System.out.println("Display By Id -unfortunate"+e.getErrorMessage());
		}
		return demo;
	}

	public String updateEmployee(int eid, Demo demoResponse) {
		try {
			return daoObject.update(eid,demoResponse);
		} catch (ApplicationException e) {
			System.out.println("unfortunate in Update "+e.getErrorMessage());
		}
		return null;
	}

	public String deleteEmployee(int eid) {

		try {
			return daoObject.delete(eid);
		} catch (ApplicationException e) {
			System.out.println("unfortunate in Update "+e.getErrorMessage());
		}
		return null;
	}
}
